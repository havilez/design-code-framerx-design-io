import * as React from "react";
import styled from "styled-components"

const Container = styled.div`
    height: 100%;
    color: white;
    font-weight: bold;
    display: flex;
    align-items: center;
    padding: 20px 0;
    font-size: 20px;
`

// Define type of property
interface Props {
    region: string;
}

interface State {
    name: string
}

export class Name extends React.Component<Props> {

    // Set default properties
    static defaultProps = {
        region: "United States"
    }

    state = {
        name: "Jane Doe"
    }

    componentDidMount = () => {
        const url = "https://uinames.com/api/?region=" + this.props.region
        fetch(url)
        .then(response => response.json()
        .then(response => {
            this.setState({
                name: response.name + " " + response.surname
            })
        }))
    }

    render() {

    return <Container>{this.state.name}</Container>;
    }
}

export default Name
