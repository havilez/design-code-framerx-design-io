import React from 'react'
import { Link } from 'gatsby'
import Icon from '../components/Icon'
import Avatar from '../components/Avatar'
import Name from '../components/Name'

import Layout from '../components/layout'

const IndexPage = () => (
  <Layout>
    <Icon />
    <Avatar />
    <Name />
  </Layout>
)

export default IndexPage
